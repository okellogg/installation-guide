# Translation of the Debian installation-guide into Indonesian.
# Andika Triwidada <andika@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: d-i-manual_preface\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2016-12-29 08:03+0000\n"
"PO-Revision-Date: 2022-03-10 12:59+0000\n"
"Last-Translator: Andika Triwidada <andika@gmail.com>\n"
"Language-Team: Debian Indonesia Translators <debian-l10n-indonesian@lists."
"debian.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.12-dev\n"

#. Tag: title
#: preface.xml:5
#, no-c-format
msgid "Installing &debian-gnu; &release; for &architecture;"
msgstr "Memasang &debian-gnu; &release; untuk &architecture;"

#. Tag: para
#: preface.xml:6
#, no-c-format
msgid ""
"We are delighted that you have decided to try &debian;, and are sure that "
"you will find that &debian;'s GNU/&arch-kernel; distribution is unique. "
"&debian-gnu; brings together high-quality free software from around the "
"world, integrating it into a coherent whole. We believe that you will find "
"that the result is truly more than the sum of the parts."
msgstr ""
"Kami senang Anda telah memutuskan untuk mencoba &debian;, dan yakin Anda "
"akan menjumpai bahwa distribusi GNU/&arch-kernel; &debian; itu unik. &debian-"
"gnu; menyatukan perangkat lunak bebas berkualitas tinggi dari seluruh dunia, "
"mengintegrasikannya ke dalam satu kesatuan yang koheren. Kami percaya bahwa "
"Anda akan menemukan bahwa hasilnya benar-benar lebih dari jumlah bagian-"
"bagiannya."

#. Tag: para
#: preface.xml:15
#, no-c-format
msgid ""
"We understand that many of you want to install &debian; without reading this "
"manual, and the &debian; installer is designed to make this possible. If you "
"don't have time to read the whole Installation Guide right now, we recommend "
"that you read the Installation Howto, which will walk you through the basic "
"installation process, and links to the manual for more advanced topics or "
"for when things go wrong. The Installation Howto can be found in <xref "
"linkend=\"installation-howto\"/>."
msgstr ""
"Kami memahami bahwa banyak dari Anda ingin menginstal &debian; tanpa membaca "
"manual ini, dan installer &debian; dirancang untuk memungkinkan hal ini. "
"Jika Anda tidak punya waktu untuk membaca seluruh Panduan Instalasi "
"sekarang, kami sarankan Anda membaca Cara Instalasi, yang akan memandu Anda "
"melalui proses instalasi dasar, dan tautan ke manual untuk topik lebih "
"lanjut atau ketika ada masalah . Cara Instalasi dapat ditemukan di <xref "
"linkend=\"installation-howto\"/>."

#. Tag: para
#: preface.xml:25
#, no-c-format
msgid ""
"With that said, we hope that you have the time to read most of this manual, "
"and doing so will lead to a more informed and likely more successful "
"installation experience."
msgstr ""
"Dengan demikian, kami berharap Anda memiliki waktu untuk membaca sebagian "
"besar manual ini, dan hal itu akan menghasilkan pengalaman instalasi yang "
"lebih terinformasi dan kemungkinan lebih berhasil."
