# translation of preface.po to Norwegian nynorsk
#
# Håvard Korsvoll <korsvoll@skulelinux.no>, 2008.
# Yngve Spjeld-Landro <l10n@landro.net>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: preface\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2016-12-29 08:03+0000\n"
"PO-Revision-Date: 2020-02-17 00:50+0000\n"
"Last-Translator: Yngve Spjeld-Landro <l10n@landro.net>\n"
"Language-Team: Norwegian nynorsk <i18n-nn@lister.ping.uio.no>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11-dev\n"

#. Tag: title
#: preface.xml:5
#, no-c-format
msgid "Installing &debian-gnu; &release; for &architecture;"
msgstr "Installasjon av &debian-gnu; &release; for &architecture;"

#. Tag: para
#: preface.xml:6
#, no-c-format
msgid ""
"We are delighted that you have decided to try &debian;, and are sure that "
"you will find that &debian;'s GNU/&arch-kernel; distribution is unique. "
"&debian-gnu; brings together high-quality free software from around the "
"world, integrating it into a coherent whole. We believe that you will find "
"that the result is truly more than the sum of the parts."
msgstr ""
"Vi er glade for at du har valt å prøve &debian; og vi er sikre på at du kjem "
"til å synast at &debian; sin GNU/&arch-kernel; distribusjon er unik. &debian-"
"gnu; fører saman høgkvalitets fri programvare frå heile verda og integrerer "
"dei i ein samanhengande heilskap. Vi trur at du vil finne at resultatet er "
"meir enn summen av delane."

#. Tag: para
#: preface.xml:15
#, no-c-format
msgid ""
"We understand that many of you want to install &debian; without reading this "
"manual, and the &debian; installer is designed to make this possible. If you "
"don't have time to read the whole Installation Guide right now, we recommend "
"that you read the Installation Howto, which will walk you through the basic "
"installation process, and links to the manual for more advanced topics or "
"for when things go wrong. The Installation Howto can be found in <xref "
"linkend=\"installation-howto\"/>."
msgstr ""
"Vi skjønar at mange av dykk ønskjer å installere &debian; utan å lese denne "
"manualen først og installasjonsprogrammet til &debian; (&debian; installer) "
"er laga for å mogleggjere det. Viss du ikkje har tid til å lese heile "
"installasjonsmanualen no, så rår vi deg å lese installasjonsrettleiinga. Ho "
"vil hjelpe deg gjennom ein grunnleggjande installasjonsprosess. Ho inneheld "
"også peikarar til manualen for meir avanserte emne eller for ein stad å sjå "
"når ting går gale. Installasjonsrettleiinga finn du i <xref linkend="
"\"installation-howto\"/>."

#. Tag: para
#: preface.xml:25
#, no-c-format
msgid ""
"With that said, we hope that you have the time to read most of this manual, "
"and doing so will lead to a more informed and likely more successful "
"installation experience."
msgstr ""
"Når det er sagt, så håpar vi du har tid til å lese mesteparten av denne "
"manualen. Ved å gjere det vil du truleg få ei meir opplyst og truleg betre "
"installasjonsoppleving."
